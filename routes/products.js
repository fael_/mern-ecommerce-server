const express = require('express');
const router = express.Router();

const Product = require('../models/Products')

const multer = require('multer');

const storage = multer.diskStorage({
	destination : function(req,file,cb){
		cb(null, "public/products")
	},
	filename : function(req,file,cb){
		cb(null, Date.now() + file.originalname)
	}
})

const upload = multer({storage : storage});

// let myfunction = function(req,res,next) {
// 	if(req.body.message == 'nice'){
// 		next();
// 	} else {
// 		res.status(403).json({message: "Unauthorized request"})
// 	};
// }
// 	console.log('May request!');

// 	router.use(myfunction);


// index
// router.get('/',function(req,res,next){
// 	// res.json({
// 	// 	data : 'get request index products'})
// 	ProductModel.find()
// 	.then(products => {
// 		res.json(products)
// 	})
// 	.catch(next)
// });

router.get('/',function(req,res,next){
	Product.find()
	.then(products => {res.json(products)})
	.catch(next)
});

// single
// router.get('/:id', (req,res,next) => {
// 	// res.json({
// 	// 	data : req.params.id
// 	// });
// 	ProductModel.findOne({_id : req.params.id})
// 	.then(product => res.json(product))
// 	.catch(next)
// });

router.get('/:id', (req,res,next) => {
	Product.findById(req.params.id)
	.then(product => res.json(product))
	.catch(next)
});

// create
// router.post('/', (req,res,next) => {
// 	// res.json({
// 	// 	data : req.body
// 	// })
// 	ProductModel.create(
// 		{
// 			name : req.body.name,
// 			categoryId : req.body.categoryId,
// 			price : req.body.price,
// 			description : req.body.description,
// 			image : req.body.image
// 		}
// 	)
// 	.then((product) => res.json(product))
// 	.catch(next)
// })

router.post('/', upload.single('image'), (req,res,next) => {
	// res.json(req.file)
	req.body.image = "/public/" + req.file.filename;
	Product.create(req.body)
	.then((product) => res.json(product))
	.catch(next)
})

// update
// router.put('/:id', (req,res,next) => {
// 	// res.json({
// 	// 	data : "This is a put request",
// 	// 	id : req.params.id,
// 	// 	body : req.body
// 	// })
// 	ProductModel.findOneAndUpdate(
// 		{
// 			name : req.body.name,
// 			categoryId : req.body.categoryId,
// 			price : req.body.price,
// 			description : req.body.description,
// 			image : req.body.image
// 		}
// 	)
// 	.then(product => res.json(product))
// 	.catch(next)
// })

router.put('/:id', (req,res,next) => {
	Product.findByIdAndUpdate(req.params.id, req.body, {new:true})
	.then(product => res.json(product))
	.catch(next)
})

// delete
// router.delete('/:id', (req,res,next) => {
// 	// res.json({
// 	// 	data : "this is a delete request",
// 	// 	id : req.params.id + " to be deleted"
// 	// })
// 	ProductModel.findOneAndDelete({_id : req.params.id})
// 	.then(product => res.json(product))
// 	.catch(next)
// })

router.delete('/:id', (req,res,next) => {
	Product.deleteOne({_id : req.params.id})
	.then(product => res.json(product))
	.catch(next)
})

module.exports = router;