const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	name : {
		type : String,
		required : [true, "Product Name is required"]
	},
	categoryId : {
		type : String,
		required : [true, "Product CategoryId is required"]
	},
	price : {
		type : Number,
		required : [true, "Product Price is required"]
	},
	description : {
		type : String,
		required : [true, "Product Description is required"]
	},
	image : {
		type : String,
		required : [true, "Product Image is required"]
	}
});

module.exports = mongoose.model('Product', ProductSchema);